const db = require('../models/index.js')

/***************** Verificar se o user existe *****************/
const validateUserAlreadyExists = async (id_skill, id) => {
  const result = await db.Allocation.findOne({
    where: {
      id,
      id_skill
    }
  })
  return result ? true : false
}

/***************** Verificar se a allocation existe *****************/
const allocationIfTheIdAlreadyExists = async id_allocation => {
  const result = await db.Allocation.findOne({
    where: {
      id: id_allocation
    }
  })
  return result ? true : false
}
/***************** Verificar se a allocation pertence ao user *****************/
const validateAllocationByUser = async (id_allocation, id_user) => {
  const allocations = await db.Allocation.findOne({
    where: {
      id: id_allocation
    }
  })
  return allocations.id_user === id_user ? true : false
}

/***************************** Criar a allocation  ****************************/
const createAllocation = (id_skill, id_user) => {
  return db.Allocation.create({
    id_skill,
    id_user
  })
}

/***************************** Deletar a allocation  ***************************/
const deleteAllocation = async id_allocation => {
  return db.Allocation.destroy({
    where: {
      id: id_allocation
    }
  })
}



module.exports = {
  allocationIfTheIdAlreadyExists,
  createAllocation,
  validateAllocationByUser,
  deleteAllocation,
  validateUserAlreadyExists
}
