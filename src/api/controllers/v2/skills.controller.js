const db = require('../../models/index.js')

const {
  listByProfileUser,
  validateIfTheNameAlreadyExists,
  UpdateSkill,
  searchSkillByProfileUser,
  validateIfTheIdAlreadyExists,
  deleteSkill,
  createSkill
} = require('../../services/skill.service')

const getAllSkills = async (request, response) => {
  const result = await listByProfileUser(request.user.type, request.user.id)

  response.status(200).send(result)
}

const getSkillById = async (request, response) => {
  // try {
    const { params, user } = request
    const result = await searchSkillByProfileUser(
      params.id,
      user.type,
      user.id
    )
  

    return response.status(200).send(result)
  // } catch (error) {
  //   return response.status(500).send({ message: 'Internal server error!!' })
  // }
}

const createSkills = async (request, response) => {
  try {
    const { skill, image} = request.body

    const result = await validateIfTheNameAlreadyExists(skill)
    if (result) {
      return response
        .status(400)
        .send({ error: 'There is already a skill with this name.' })
    }

    await createSkill(skill, image)

    return response.status(200).send({ message: 'Skill successfully created.' })
  } catch (error) {
    return response.status(500).send({ message: 'Internal server error!!' })
  }
}

const updateSkills = async (request, response) => {
  try {
    const { id_skill } = request.params
    const { skill, image, status } = request.body

    await UpdateSkill(id_skill, { skill, image, status })

    return response.status(200).send({ message: 'Skill successfully change' })
  } catch (error) {
    return response.status(500).send({ message: 'Internal server error!!' })
  }
}

const deleteSkills = async (request, response) => {
  try {
    const { id_skill } = request.params
    const skillAlreadyExists = await validateIfTheIdAlreadyExists(id_skill)

    if (!skillAlreadyExists) {
      return response.status(422).send({ message: 'Skill not found.' })
    }

    await deleteSkill(id_skill)
    return response.status(200).send({ message: 'Skill successfully deleted' })
  } catch (error) {
    return response.status(500).send({ message: 'Internal server error!!' })
  }
}

module.exports = {
  getAllSkills,
  getSkillById,
  createSkills,
  updateSkills,
  deleteSkills
}
