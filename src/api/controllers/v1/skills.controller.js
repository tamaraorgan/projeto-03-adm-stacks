const db = require('../../models/index.js')

const getAllSkills = async (request, response) => {
  const result = await db.Skill.findAll({})

  response.status(200).send(
    result.map(item => {
      const { id, skill, image } = item

      return {
        id,
        skill,
        image
      }
    }) || []
  )
}

const getSkillById = async (request, response) => {
  try {
    const result = await db.Skill.findOne({
      where: {
        id: request.params.id
      },
      include: {
        model: db.Allocation,
        as: 'allocations'
      }
    })

    return response.status(200).send({
      id: result.id,
      skill: result.skill,
      image: result.image,
      icon: result.icon,
      allocation: result.allocation
    })
  } catch (error) {
    return response.status(500).send({ message: 'Internal server error!!' })
  }
}
const createSkills = async (request, response) => {
  try {
    const { skill, image } = request.body
    const result = await db.Skill.create({
      skill,
      image,
      status
    })

    response.json(result)
  } catch (error) {
    response.status(500).send({ message: 'Internal server error!!' })
  }
}

module.exports = {
  getAllSkills,
  getSkillById,
  createSkills
}
