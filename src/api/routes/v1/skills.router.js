const skillsController = require('../../controllers/v1/skills.controller')

module.exports = router => {
  router.route('/skills').get(skillsController.getAllSkills)
  router.route('/skills').post(skillsController.createSkills)

  router.route('/skills/:id').get(skillsController.getSkillById)
}
