const allocationsController = require('../../controllers/v1/allocation.controller')

module.exports = router => {
  router
    .route('/skills/:id/allocation')
    .get(allocationsController.getAllocation)

  router
    .route('/skills/:id/allocation')
    .post(allocationsController.postAllocationSkill)

  router
    .route('/skills/:id_allocation')
    .delete(allocationsController.deleteAllocation)
}
