const Joi = require('joi')
const skillsController = require('../../controllers/v2/skills.controller')
const { authorization, validateDTO } = require('../../utils/validate.utils')

module.exports = router => {
  router.route('/skills').get(authorization(), skillsController.getAllSkills)

  router.route('/skills/:id').get(
    authorization(),
    validateDTO('params', {
      id: Joi.number().integer().required().messages({
        'any.required': 'ID é obrigatório.',
        'number.base': 'ID deve ser um número',
        'number.integer': 'ID deve ser um número válido'
      })
    }),
    skillsController.getSkillById
  )

  router.route('/skills').post(
    authorization('CREATE_SKILLS'),
    validateDTO('body', {
      skill: Joi.string().min(4).required(),
      image: Joi.string(),
      status: Joi.string()
    }),
    skillsController.createSkills
  )

  router.route('/skills/:id_skill').put(
    authorization('UPDATE_SKILLS'),
    validateDTO('params', {
      id_skill: Joi.number().integer().required().messages({
        'any.required': 'ID é obrigatório.',
        'number.base': 'ID deve ser um número',
        'number.integer': 'ID deve ser um número válido'
      })
    }),
    validateDTO('body', {
      skill: Joi.string().min(4).required(),
      image: Joi.string(),
      status: Joi.boolean()
    }),
    skillsController.updateSkills
  )
  router.route('/skills/:id_skill').delete(
    authorization(),
    validateDTO('params', {
      id_skill: Joi.number().integer().required().messages({
        'any.required': 'ID é obrigatório.',
        'number.base': 'ID deve ser um número',
        'number.integer': 'ID deve ser um número válido'
      })
    }),
    skillsController.deleteSkills
  )
}
