const Joi = require('joi')

const userController = require('../../controllers/v2/users.controller')
const { validateDTO, authorization } = require('../../utils/validate.utils')

module.exports = router => {
  router.route('/sessions').post(
    validateDTO('body', {
      user: Joi.string().email().required().messages({
        'any.required': 'E-mail é um campo obrigatório.',
        'string.empty': 'E-mail não pode ser vazio.',
        'string.email': 'E-mail deve ser válido'
      }),
      password: Joi.string().min(6).max(12).required().messages({
        'any.required': 'A senha é um campo obrigatório.',
        'string.empty': 'A senha não pode ser vazia.',
        'string.min': 'A senha deve conter no mínimo 6 caracteres',
        'string.max': 'A senha deve conter no máximo 12 caracteres'
      })
    }),
    userController.authUser
  )

  router.route('/users').post(
    validateDTO('body', {
      name: Joi.string().min(6).max(30).required().messages({
        'any.required': 'O nome é um campo obrigatório.',
        'string.empty': 'O nome não pode ser vazio.',
        'string.min': 'O nome deve conter no mínimo 6 caracteres',
        'string.max': 'O nome deve conter no máximo 30 caracteres'
      }),
      email: Joi.string().email().required().messages({
        'any.required': 'E-mail é um campo obrigatório.',
        'string.empty': 'E-mail não pode ser vazio.',
        'string.email': 'E-mail deve ser válido'
      }),
      password: Joi.string().min(6).max(12).required().messages({
        'any.required': 'A senha é um campo obrigatório.',
        'string.empty': 'A senha não pode ser vazia.',
        'string.min': 'A senha deve conter no mínimo 6 caracteres',
        'string.max': 'A senha deve conter no máximo 12 caracteres'
      }),
      type: Joi.string()
    }),
    userController.createUser
  )
  router
    .route('/users')
    .get(authorization('LIST_SKILLS'), userController.getAllUsers)

  router
    .route('/users/:id')
    .get(authorization('LIST_USERS'), userController.listUsersById)

  router.route('/users/:id').put(
    authorization('UPDATE_USERS'),
    validateDTO('params', {
      id: Joi.number().integer().required().messages({
        'any.required': 'ID é obrigatório.',
        'number.base': 'ID deve ser um número',
        'number.integer': 'ID deve ser um número válido'
      })
    }),
    validateDTO('body', {
      name: Joi.string().min(6).max(30).required().messages({
        'any.required': 'O nome é um campo obrigatório.',
        'string.empty': 'O nome não pode ser vazio.',
        'string.min': 'O nome deve conter no mínimo 6 caracteres',
        'string.max': 'O nome deve conter no máximo 30 caracteres'
      }),
      email: Joi.string().email().required().messages({
        'any.required': 'E-mail é um campo obrigatório.',
        'string.empty': 'E-mail não pode ser vazio.',
        'string.email': 'E-mail deve ser válido'
      }),
      type: Joi.string()
    }),
    userController.updateUsers
  )

  router
    .route('/employees')
    .get(authorization('LIST_USERS_SKILLS'), userController.listUsersAndSkills)
}
