const Joi = require('joi')
const allocationController = require('../../controllers/v2/allocation.controller')
const { authorization, validateDTO } = require('../../utils/validate.utils')

module.exports = router => {
  router
    .route('/skills/:id_skill/allocation')
    .get(authorization(), allocationController.getAllocation)

  router.route('/skills/:id_skill/allocation').post(
    authorization('CREATE_ALLOCATIONS'),
    validateDTO('params', {
      id_skill: Joi.number().integer().required().messages({
        'any.required': 'ID é obrigatório.',
        'number.base': 'ID deve ser um número',
        'number.integer': 'ID deve ser um número válido'
      })
    }),
    allocationController.postAllocationSkill
  )

  router.route('/skills/allocation/:id_allocation').delete(
    authorization('DELETE_ALLOCATIONS'),
    validateDTO('params', {
      id_allocation: Joi.number().integer().required().messages({
        'any.required': 'ID é obrigatório.',
        'number.base': 'ID deve ser um número',
        'number.integer': 'ID deve ser um número válido'
      })
    }),

    allocationController.deleteAllocations
  )
}
