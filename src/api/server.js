//imports
const express = require('express')
const cors = require('cors')
const router = require('./routes')

const app = express()
const port = process.env.PORT || 3330

app.use(express.json())
app.use(cors())

router(app)

app.listen(port)

module.exports = app