module.exports = (sequelize, DataTypes) => {
  const Allocation = sequelize.define(
    'Allocation',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        unique: true,
        allowNull: false
      },
      employee_name: DataTypes.TEXT,
      employee_email: DataTypes.TEXT,
      employee_phone: DataTypes.TEXT,
      employee_local: DataTypes.TEXT,
      employee_state: DataTypes.TEXT
    },
    {
      undescored: true,
      paranoid: true,
      timestamps: false,
      tableName: 'tallocation'
    }
  )

  Allocation.associate = function (models) {
    Allocation.belongsTo(models.User, {
      foreignKey: 'id_user',
      as: 'users'
    })
    Allocation.belongsTo(models.Skill, {
      foreignKey: 'id_skill',
      as: 'skills'
    })
  }

  return Allocation
}