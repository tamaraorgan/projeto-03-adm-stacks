const Joi = require('joi')
const jwt = require('jsonwebtoken')
const { searchUsersToEmailService } = require('../services/user.service')

const profiles = [
  {
    id: '1',
    functionality: [
      'CREATE_SKILLS',
      'UPDATE_SKILLS',
      'LIST_SKILLS',
      'LIST_USERS_SKILLS',
      'LIST_USERS',
      'UPDATE_USERS'
    ]
  },
  {
    id: '2',
    functionality: [
      'UPDATE_USERS',
      'CREATE_ALLOCATIONS',
      'DELETE_ALLOCATIONS',
      'LIST_USERS'
    ]
  }
]
/********************** CRIA DETALHES PARA ERROR VALIDATEDTO ******************/
const searchProfileById = id => {
  const profile = profiles.find(item => Number(item.id) === Number(id))

  return profile
}

/********************** CRIA DETALHES PARA ERROR VALIDATEDTO ******************/
const createDetail = error => {
  return error.details.reduce((acc, item) => {
    return [...acc, item.message]
  }, [])
}

/********************************* VALIDATEDTO ********************************/
const validateDTO = (type, params) => {
  return (request, response, next) => {
    try {
      const schema = Joi.object().keys(params)

      const { value, error } = schema.validate(request[type], {
        allowUnknown: false
      })
      request[type] = value
      return error
        ? response.status(422).send({
            detail: [...createDetail(error)]
          })
        : next()
    } catch (error) {
      return response.status(500).send({ error: 'Internal server error!!' })
    }
  }
}

/************************** AUTORIZAÇÃO DO USER *******************************/
const authorization = (route = '*') => {
  return async (request, response, next) => {
    const { token } = request.headers

    try {
      if (!token) {
        response.status(403).send({ error: 'Unauthorized user.' })
      }
      const userJwt = jwt.verify(token, process.env.JWT_KEY)

      const user = await searchUsersToEmailService(userJwt.email)
      request.user = user

      if (route !== '*') {
        const profile = searchProfileById(user.type)
        if (!profile.functionality.includes(route)) {
          return response.status(403).send({ error: 'Unauthorized user' })
        }
      }

      next()
    } catch (error) {
      return response.status(401).send({ error: 'Unauthorized user.' })
    }
  }
}

module.exports = {
  searchProfileById,
  createDetail,
  validateDTO,
  authorization
}