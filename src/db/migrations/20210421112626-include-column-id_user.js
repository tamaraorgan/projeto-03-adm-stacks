'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('tallocation', 'id_user', Sequelize.INTEGER, {
        allowNull: true
      }),
      queryInterface.addConstraint("tallocation", {
        fields: ["id_user"],
        type: "foreign key",
        name: "tallocation_user_fk",
        references: {
          table: "tuser",
          field: "id",
        },
        onDelete: "CASCADE",
      }),
    ])
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeConstraint('tallocation','tallocation_users_fk'),
      queryInterface.removeColumn('tallocation', 'id_user'),
    ]);
  }
}
