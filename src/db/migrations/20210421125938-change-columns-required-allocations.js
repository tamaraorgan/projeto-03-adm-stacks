'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([

      queryInterface.changeColumn('tallocation', 'employee_name', {
        type: Sequelize.TEXT
      }, {
        allowNull: true,
      }),

      queryInterface.changeColumn('tallocation', 'employee_email', {
        type: Sequelize.TEXT
      }, {
        allowNull: true,
      }),
      queryInterface.changeColumn('tallocation', 'employee_phone', {
        type: Sequelize.TEXT
      }, {
        allowNull: true,
      }),
      queryInterface.changeColumn('tallocation', 'employee_local', {
        type: Sequelize.TEXT
      }, {
        allowNull: true,
      }),
      queryInterface.changeColumn('tallocation', 'employee_state', {
        type: Sequelize.TEXT
      }, {
        allowNull: true,
      })
    ])
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([

      queryInterface.changeColumn('tallocation', 'employee_name', {
        type: Sequelize.TEXT
      }, {
        allowNull: false,
      }),

      queryInterface.changeColumn('tallocation', 'employee_email', {
        type: Sequelize.TEXT
      }, {
        allowNull: false,
      }),
      queryInterface.changeColumn('tallocation', 'employee_phone', {
        type: Sequelize.TEXT
      }, {
        allowNull: false,
      }),
      queryInterface.changeColumn('tallocation', 'employee_local', {
        type: Sequelize.TEXT
      }, {
        allowNull: false,
      }),
      queryInterface.changeColumn('tallocation', 'employee_state', {
        type: Sequelize.TEXT
      }, {
        allowNull: false,
      })
    ])
  }
};


